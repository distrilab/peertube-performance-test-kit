#!/bin/bash

DURATION=300
RESOLUTION=1280x720
VBITRATE=4096
ABITRATE=160

if [[ $# -lt 1 ]] ; then
  echo "Not enough arguments supplied" >&2
  echo "You need to pass at least the duration in seconds " >&2
  echo " " >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <duration-in-sec> <resolution> <video-bitrate-kbps> <audio-bitrate-kbps>" >&2
  echo " " >&2
  echo "other parameters are optional and will default to the " >&2
  echo "values in the following example :" >&2
  echo "  `basename $0` $DURATION $RESOLUTION $VBITRATE $ABITRATE" >&2
  exit 1
fi

if [[ ! -z "$1" ]]
then
  DURATION="$1"
fi

if [[ ! -z "$2" ]]
then
  RESOLUTION="$2"
fi

if [[ ! -z "$3" ]]
then
  VBITRATE="$3"
fi

BUFFERSIZE=$(echo "2*${VBITRATE}" | bc)

if [[ ! -z "$4" ]]
then
  ABITRATE="$4"
fi


FILENAME=random-noise-${RESOLUTION}p-${DURATION}s-${VBITRATE}kbps-${ABITRATE}kbps.mkv
echo " "
echo Will generate random noise stream for $DURATION seconds to file : 
echo $FILENAME
echo with :
echo "- $RESOLUTION resolution, "
echo "- $VBITRATE kbit/s constant video bitrate, "
echo "- $BUFFERSIZE kbit video buffer and "
echo "- $ABITRATE kbit/s audio bitrate"
echo " "


#-channel_layout stereo \

rm $FILENAME

ffmpeg \
-loglevel level \
-f rawvideo \
-video_size ${RESOLUTION} \
-pixel_format yuv420p \
-framerate 30 -i /dev/urandom \
-ar 48000 \
-ac 2 \
-f s16le \
-channel_layout stereo \
-i /dev/urandom \
-filter_complex "[1:a]volume=-30dB;[0:v]drawtext='font=DejaVuSansMono:text=%{pts}: fontcolor=white: fontsize=h/7: box=1: boxcolor=black@0.73: boxborderw=5: x=mod(floor(t/10+1)\,2)*mod(t*(w-text_w)/10\,w-text_w)+mod(floor(t/10)\,2)*(w-text_w-mod(t*(w-text_w)/10\,w-text_w)): y=h/2-3*text_h/2'" \
-codec:v libx264 \
-preset superfast \
-x264opts nal-hrd=cbr:force-cfr=1:vbv-bufsize=${BUFFERSIZE}:bitrate=${VBITRATE}:vbv-maxrate=${VBITRATE} \
-c:a aac -b:a ${ABITRATE}k \
-t ${DURATION} \
${FILENAME}
