# PeerTube Performances Test Kit

Scripts to generate video files and live streams for performance testing of PeerTube

## Dependencies
These scripts rely on :
- `ffmpeg` to generate the audio/video streams. Tested with ffmpeg versions:  
  - 4.1.6
  - 4.2.4
- `bc` to compute the encoding buffer size from the specified video bitrate

To install the dependencies on Debian 10 (Buster):  
`sudo apt install ffmpeg bc`

# Mains scripts
There are 4 main scripts : 
- gen_pattern_sine.sh  
    generate video file with static pattern, time overlay (seconds elapsed from start) and sine wave stereo audio
- gen_random_noise.sh  
    generate video file with random video, time overlay (seconds elapsed from start and local timestamp) and sine wave stereo audio
- stream_pattern_sine.sh  
    send live stream and save a copy to file with static pattern, time overlay (seconds from start) and sine wave audio
- stream_random_noise.sh  
    send live stream and save a copy to file with random video, time overlay (seconds elapsed from start and local timestamp) and sine wave audio

Run the scripts with no parameter for help.

The "random_noise" scripts are designed to produce video streams that are very hard to encode. These are to test worst case scenarii

The "pattern_sine" scripts produce video streams that are lighter to process (use less CPU to encode on peertube side) and compress to a smaller size.

# Helper scripts
The other scripts are helpers that are just sortcuts to call the main scripts with some pre-defined parameters values. The names of these scripts should be self explanatory :

- gen_pattern_1080p_60s.sh
- gen_pattern_720p_60s.sh
- gen_random_1080p_60s.sh
- gen_random_720p_60s.sh
- stream_pattern_1080p_300s.sh
- stream_pattern_1080p_3600s.sh
- stream_pattern_720p_300s.sh
- stream_pattern_720p_3600s.sh
- stream_random_1080p_300s.sh
- stream_random_1080p_3600s.sh
- stream_random_720p_300s.sh
- stream_random_720p_3600s.sh


