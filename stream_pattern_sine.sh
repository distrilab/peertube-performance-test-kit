#!/bin/bash

DURATION=300
RESOLUTION=1280x720
VBITRATE=4096
ABITRATE=160

if [[ $# -lt 2 ]] ; then
  echo "Not enough arguments supplied" >&2
  echo "You need to pass at least the PeerTube server name as the " >&2
  echo "first parameter and the streaming key as the second" >&2
  echo " " >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <peertube-server> <streaming-key> <duration-in-sec> <resolution> <video-bitrate-kbps> <audio-bitrate-kbps>" >&2
  echo " " >&2
  echo "other parameters are optional and will default to the " >&2
  echo "values in the following example :" >&2
  echo "  `basename $0` tube.example.com 69ac2d29-eba4-4d5f-a3a6-6047e6768ffb $DURATION $RESOLUTION $VBITRATE $ABITRATE" >&2
  exit 1
fi

URL=rtmp://$1:1935/live/$2

if [[ ! -z "$3" ]]
then
  DURATION="$3"
fi

if [[ ! -z "$4" ]]
then
  RESOLUTION="$4"
fi

if [[ ! -z "$5" ]]
then
  VBITRATE="$5"
fi

BUFFERSIZE=$(echo "2*${VBITRATE}" | bc)

if [[ ! -z "$6" ]]
then
  ABITRATE="$6"
fi

FILENAME=live-pattern-${1}-${RESOLUTION}p-${DURATION}s-${VBITRATE}kbps-${ABITRATE}kbps-${2}.mkv
echo " "
echo Will stream to $URL for $DURATION seconds
echo and save a copy of the data sent to the peertube server to file :
echo $FILENAME
echo with :
echo "- $RESOLUTION resolution, "
echo "- $VBITRATE kbit/s constant video bitrate, "
echo "- $BUFFERSIZE kbit video buffer and "
echo "- $ABITRATE kbit/s audio bitrate"
echo " "
$

#-x264opts nal-hrd=cbr:force-cfr=1:vbv-bufsize=${BUFFERSIZE}:bitrate=${VBITRATE}:vbv-maxrate=${VBITRATE} \
#-shortest \

ffmpeg \
-loglevel level \
-s ${RESOLUTION} \
-loop 1 \
-r 30 \
-y \
-ac 2 \
-i Mire_1080p.png \
-re \
-f lavfi \
-i aevalsrc="(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360-2.5/2)*t)+0.1*sin(2*PI*(360-2.5/2)*t)|(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360+2.5/2)*t)+0.1*sin(2*PI*(360+2.5/2)*t):sample_rate=48000" \
-channel_layout stereo \
-ar 48000 \
-pixel_format yuv420p \
-filter_complex "[1:a]volume=-10dB;[0:v]scale='size=${RESOLUTION}',drawtext='font=DejaVuSansMono:text=%{pts}: fontcolor=white: fontsize=h/7: box=1: boxcolor=black@0.73: boxborderw=5: x=mod(floor(t/10+1)\,2)*mod(t*(w-text_w)/10\,w-text_w)+mod(floor(t/10)\,2)*(w-text_w-mod(t*(w-text_w)/10\,w-text_w)): y=h/2-3*text_h/2', drawtext='font=DejaVuSansMono:text=%{localtime}: fontcolor=white: fontsize=h/7: box=1: boxcolor=black@0.73: boxborderw=5: x=(w-text_w)/2: y=h/2+3*text_h/2'" \
-codec:v libx264 \
-preset superfast \
-x264opts nal-hrd=cbr:force-cfr=1:vbv-bufsize=${BUFFERSIZE}:bitrate=${VBITRATE}:vbv-maxrate=${VBITRATE} \
-c:a aac -b:a ${ABITRATE}k \
-t ${DURATION} \
-flags +global_header \
-f tee -map 0:v -map 1:a "[f=matroska]${FILENAME}|[f=flv]${URL}"
