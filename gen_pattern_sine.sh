#!/bin/bash

DURATION=300
RESOLUTION=1280x720
VBITRATE=4096
ABITRATE=160

if [[ $# -lt 1 ]] ; then
  echo "Not enough arguments supplied" >&2
  echo "You need to pass at least the duration in seconds " >&2
  echo " " >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <duration-in-sec> <resolution> <video-bitrate-kbps> <audio-bitrate-kbps>" >&2
  echo " " >&2
  echo "other parameters are optional and will default to the " >&2
  echo "values in the following example :" >&2
  echo "  `basename $0` $DURATION $RESOLUTION $VBITRATE $ABITRATE" >&2
  exit 1
fi

if [[ ! -z "$1" ]]
then
  DURATION="$1"
fi

if [[ ! -z "$2" ]]
then
  RESOLUTION="$2"
fi

if [[ ! -z "$3" ]]
then
  VBITRATE="$3"
fi

BUFFERSIZE=$(echo "2*${VBITRATE}" | bc)

if [[ ! -z "$4" ]]
then
  ABITRATE="$4"
fi


FILENAME=pattern-sine-${RESOLUTION}p-${DURATION}s-${VBITRATE}kbps-${ABITRATE}kbps.mkv
echo " "
echo Will generate image pattern with sine wave stream for $DURATION seconds to file : 
echo $FILENAME
echo with :
echo "- $RESOLUTION resolution, "
echo "- $VBITRATE kbit/s constant video bitrate, "
echo "- $BUFFERSIZE kbit video buffer and "
echo "- $ABITRATE kbit/s audio bitrate"
echo " "


rm $FILENAME


ffmpeg \
-loglevel level \
-s ${RESOLUTION} \
-loop 1 \
-r 30 \
-y \
-ac 2 \
-i Mire_1080p.png \
-f lavfi \
-i aevalsrc="(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360-2.5/2)*t)+0.1*sin(2*PI*(360-2.5/2)*t)|(1-min(1\,mod(floor(5*t)\,50)))*0.1*sin(8*PI*(360+2.5/2)*t)+0.1*sin(2*PI*(360+2.5/2)*t):sample_rate=48000" \
-channel_layout stereo \
-ar 48000 \
-shortest \
-pixel_format yuv420p \
-filter_complex "[1:a]volume=-10dB;[0:v]scale='size=${RESOLUTION}',drawtext='font=DejaVuSansMono:text=%{pts}: fontcolor=white: fontsize=h/7: box=1: boxcolor=black@0.73: boxborderw=5: x=mod(floor(t/10+1)\,2)*mod(t*(w-text_w)/10\,w-text_w)+mod(floor(t/10)\,2)*(w-text_w-mod(t*(w-text_w)/10\,w-text_w)): y=h/2-3*text_h/2'" \
-codec:v libx264 \
-preset superfast \
-x264opts bitrate=${VBITRATE} \
-c:a aac -b:a ${ABITRATE}k \
-t ${DURATION} \
${FILENAME}
